using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{

    public GameObject pausePlael;
    bool isPause = false;
    // Start is called before the first frame update
    void Start()
    {
        pausePlael.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }
    }

    public void TogglePause() 
    { 
         isPause= !isPause;
        if (isPause)
        {
            //stop time
            pausePlael.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            //sart time 
            pausePlael.SetActive(false);
            Time.timeScale = 1;
        }
    
    }
}
