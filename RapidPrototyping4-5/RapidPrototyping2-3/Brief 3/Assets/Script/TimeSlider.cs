using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeSlider : MonoBehaviour
{
    public  Slider timerSlider;
    public float sliderTimer = 55f;
    public bool stoptime = false;
    public GameObject enemy;
    public GameObject ht;
   // private int dame = 1;
    
    //public float timeStar;
    // Start is called before the first frame update
    void Start()
    {
        //maxValue = 13;
        //Invoke(OnEmerdHit);
        // fin OnEmerdHit();
        timerSlider.maxValue = sliderTimer;
        timerSlider.minValue = 0f;
        StarTimer();
        //timerSlider = FindObjectOfType<Slider>();
    }

    public void StarTimer()
    {
        StartCoroutine(StartTheTimerTicker());
        //timeStar = 10;
        //stoptime = true;
        

    }
    public IEnumerator StartTheTimerTicker()
    {
        while (stoptime == false)
        {
            sliderTimer -= Time.deltaTime;
            yield return new WaitForSeconds(0.001f);

            if (sliderTimer <= 0)
            {
                stoptime = true;
                SpawnDame();
                StarTimer();
                Destroy(enemy);
            }
            if (stoptime == false)
            {
                timerSlider.value = sliderTimer;
               
            }


        }
        
        
        
    }
    public void StopTimer()
    {
        stoptime = true;
    }
    // Update is called once per frame
    void SpawnDame()
    {
        Instantiate(ht);
    }
    void Update()
    {
       // StarCountDown();
       // float time = timeStar - Time.deltaTime;

       // int miuntes = Mathf.FloorToInt(time / 60);
       // int seconds = Mathf.FloorToInt(time - miuntes * 60f);

       // string textTime = string.Format("{0:0}:{1:00}", miuntes, seconds);

       // timeStar -= Time.deltaTime;
       //// textBox.text = textTime;
       // timerSlider.value = timeStar;
       // if (timeStar < 0)
       // {
       //     Debug.Log("Timeoff");
       // }
    }
}
