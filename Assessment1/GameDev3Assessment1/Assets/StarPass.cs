using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarPass : MonoBehaviour
{
    
    public float waitTime = 1;
    public float movespeed = 6;
    public Transform[] movePositions;
    public Transform[] moveToPositions;
    int currentPosition = 0;
    private float powerupStrength = 95;


    // Start is called before the first frame update
    private void Start()
    {
        //PlayerContrlollerScript = FindObjectOfType<PlayerContrloller>();
        StartCoroutine(MoveDirection());
    }

    IEnumerator MoveDirection()
    {
        movespeed++;
        waitTime--;
        waitTime--;
        Vector3 newPos = movePositions[currentPosition].position;
        while (Vector3.Distance(transform.position, newPos) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, newPos, movespeed * Time.deltaTime);
            yield return null;
        }
        yield return new WaitForSeconds(waitTime);
        if (currentPosition != movePositions.Length - 1)
        {
            currentPosition = currentPosition + 1;
        }
        else
        {
            currentPosition = 0;
        }
        StartCoroutine(MoveDirection());
    }

    private void OnCollisionEnter(Collision other)
    {
        Rigidbody enemyRigidbody = other.gameObject.GetComponent<Rigidbody>();
        Vector3 awayFromPlayer = transform.position - other.gameObject.transform.position;
        if (other.gameObject.CompareTag("Player"))
        {
            movespeed--;
            movespeed--;
            movespeed--;
            movespeed--;
            movespeed--;
            waitTime--;
            waitTime--;
            waitTime--;
            powerupStrength++;
            enemyRigidbody.AddForce(-awayFromPlayer * powerupStrength, ForceMode.Impulse);
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            movespeed++;
            movespeed++;
            movespeed++;
            waitTime++;
            waitTime++;
            waitTime++;
            waitTime++;
            waitTime++;
            powerupStrength++;
            powerupStrength++;
            powerupStrength++;
            powerupStrength++;
            enemyRigidbody.AddForce(-awayFromPlayer * powerupStrength, ForceMode.Impulse);
        }
    }
}

