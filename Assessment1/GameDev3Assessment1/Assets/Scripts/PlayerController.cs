using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;
    private GameObject focalPoint;
    private float powerUpStrength = 15.0f;
    public float speed = 5.0f;
    public float powerUpTime = 4f;
    public bool hasPowerup = false;
    public GameObject PowerupIndicator;

    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        focalPoint = GameObject.Find("Focal Point");
    }

    // Update is called once per frame
    void Update()
    {
        float forwardInput = Input.GetAxis("Vertical");

        playerRb.AddForce(focalPoint.transform.forward * forwardInput * speed);

        PowerupIndicator.transform.position = transform.position + new Vector3(0, 0.5f,0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Powerup")) 
        { 
          hasPowerup = true;
          PowerupIndicator.gameObject.SetActive(true);
          Destroy(other.gameObject);
          StartCoroutine(PowerupCountdownRouytine());
        
        }
    }

    IEnumerator PowerupCountdownRouytine()
    {
        yield return new WaitForSeconds(powerUpTime);
        hasPowerup = false;
        PowerupIndicator.gameObject.SetActive(false);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Enemy") && hasPowerup) 
        { 
            Rigidbody enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
            Vector3 awayFromPlayer = collision.gameObject.transform.position - transform.position;
            powerUpStrength++;
            //powerUpTime++;

            enemyRigidbody.AddForce(awayFromPlayer * powerUpStrength, ForceMode.Impulse);
           Debug.Log("Collided With: " + collision.gameObject.name + " with powerup set to" + hasPowerup);
            powerUpTime++;
        }
        if (collision.gameObject.CompareTag("AStar"))
        {
            
           Vector3 awayFromPlayer = collision.gameObject.transform.position - transform.position;
            powerUpStrength--;
            powerUpTime--;
            speed++;
            speed++;
        }
    }
}
