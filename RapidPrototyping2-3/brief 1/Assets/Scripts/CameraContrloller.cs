using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraContrloller : MonoBehaviour
{


    public GameObject player;
    private Vector3 offset;
    //public Transform target;
    //public Vector3 rotation;
    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    private void LateUpdate()
    {
        transform.position = player.transform.position + offset;
        //transform.LookAt(target);
        //transform.localRotation = Quaternion.Euler(rotation);
    }
 
}
