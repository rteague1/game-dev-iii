using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.UI;
//using TMPro;

public class PlayerContrloller : MonoBehaviour
{
    public int collectibleCont;
    public int totalcollectibleCont;

    [Header("win Condition")]
    //public bool collectAllToWin;
    //public bool collectHalfToWin;
    //public bool collectNumToWin;
    //public int specificNunPickupToWint;
    //[Header("UI")]
    //public GameObject gameOverPanel;
    //public TMP_Text winText;
    //public TMP_Text pickupLeftText;
    //public TMP_Text pickupCollectedText;
    
    private Rigidbody rigidBody;
    public float speed = 10;

    public GameObject resetPoint;
    bool resetting = false;
    Color originalColour;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        originalColour = GetComponent<Renderer>().material.color;
        //gameOverPanel.SetActive(false);
        rigidBody = GetComponent<Rigidbody>();
       // totalcollectibleCont = GameObject.FindGameObjectsWithTag("Pickup").Length;

        //collectibleCont = 0;
        Debug.Log("pickup collected: " + collectibleCont);
        Debug.Log("pickup left: " + totalcollectibleCont);
     
        //pickupCollectedText.text = "0";
    }
    private void Update()
    {
        SetContText();
    }
    void SetContText() 
    {
        
        //pickupLeftText.text = ("pickup left: " + totalcollectibleCont);
        //if (collectAllToWin == true) ;
        //{
        //    CollectAllPickUps();
        //}
        //if (collectHalfToWin == true) 
        //{
        //    CollectMoreThanHalfPickUps();
        //}
        //if (collectNumToWin == true)
        //{
        //    CollectSpecificNumPickUps();
        //}

    }

    //void CollectAllPickUps() 
    //{
    //    if (totalcollectibleCont == 0)
    //    {
    //        WinGame();
    //    }
    //}
    //void CollectSpecificNumPickUps()
    //{
    //    if (collectibleCont == specificNunPickupToWint)
    //    {
    //        WinGame();
    //    }

    //}
    //void CollectMoreThanHalfPickUps()
    //{
    //    if (collectibleCont >= totalcollectibleCont)
    //    {
    //        WinGame();
    //    }

    //}
    // Update is called once per frame
    void FixedUpdate()
    {
        if (resetting)
        {
            return;
        }

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        //                                   x          y     z
        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);
        Vector3 movementDiretion = movement.normalized;

        if (movementDiretion != Vector3.zero)
        {
            Quaternion desredRotation = Quaternion.LookRotation(movementDiretion, Vector3.up);

            transform.rotation = desredRotation;
        }



        rigidBody.AddForce(movement * speed);
    }

    void WinGame()
    {
        //gameOverPanel.SetActive(true);
        //winText.text = "win";
    }
    private void OnCollisionEnter(Collision collison)
    {
        if (collison.gameObject.CompareTag("Respawn"))
        {
            StartCoroutine(ResetPlater());
        }
    }

    public IEnumerator ResetPlater() 
    {
      resetting= true;

        GetComponent<Renderer>().material.color = Color.red;
        rigidBody.velocity = Vector3.zero;
        Vector3 staertPot = transform.position;
        float resetSpeed = 2f;
        var i = 0.0f;
        var rate = 1.0f / resetSpeed;
        while(i<1.0f)
        {
            i += Time.deltaTime * rate;
            transform.position = Vector3.Lerp(staertPot,resetPoint.transform.position,i);
            yield return null;
        }

        GetComponent<Renderer>().material.color = originalColour;
        resetting = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Eneny"));
        {
            Destroy(other.gameObject);
            //collectibleCont+=1;
           /// totalcollectibleCont = FindObjectsOfType<Rotator>().Length - 1;
            //Debug.Log("pickup collected: " + collectibleCont);
            //pickupCollectedText.text = ("pickup collected: " + collectibleCont);
            //Debug.Log("pickup left:" + totalcollectibleCont);
            //pickupLeftText.text = ("pickup left:" + totalcollectibleCont);
        }
    }
   
}
