
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMovementX : MonoBehaviour
{
    //[SerializeField] private MoveSettings _settings = null;
    public CharacterController controller;

    public float speed = 12f;
    public float slopeSlideSpeed =50;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    private Vector3 moveDirection;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    private float groundRayDistance = 1f;
    private RaycastHit slopehit; //2
    Vector3 velocity;
    bool isGrounded;
    [Header("Slope Handling")]
    public float maxSlopeAngle;
    public float playerHeight;
    private RaycastHit slopeHit;
    private bool exitingSlope;
    Rigidbody rb;
    void Update() 
    {

        if (OnSteepSlope())
        {
            SteepSlopeMovement();
            OnSlope();
        }
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0) 
        {
            velocity.y = -2f;
        }
        if (OnSlope())
        {
            rb.AddForce(GetSlopeMoveDirection() * slopeSlideSpeed * 20f, ForceMode.Force);
        }
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move* speed * Time.deltaTime);

        //if (Input.GetButtonDown("Jump") && isGrounded) 
        //{
        //    velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        //}

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    
    }
    private void DefaultMovememnt()
    {
        if (controller.isGrounded)
        {
            //moveDirection.x = PlayerInput.move.x * _settings.speed;
            //moveDirection.z = PlayerInput.move.y * _settings.speed;
            //moveDirection.y = - settings.antiBump;
        }
    }
    private bool OnSteepSlope() 
    { 
      if(!controller.isGrounded)  return false;

      if (Physics.Raycast(transform.position,Vector3.down, out slopehit,(controller.height / 2)+ groundRayDistance)) 
        {
            float slopeAngle = Vector3.Angle(slopehit.normal, Vector3.up);
            if (slopeAngle > controller.slopeLimit) return true;
        }
        return false;
    }
    private void SteepSlopeMovement()
    {
        Vector3 slopeDirection = Vector3.up - slopehit.normal * Vector3.Dot(Vector3.up, slopehit.normal);
        float slideSpeed = speed + slopeSlideSpeed + Time.deltaTime;

        moveDirection = slopeDirection * -slideSpeed;
        moveDirection.y = moveDirection.y - slopehit.point.y;
    }
    private bool OnSlope()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out slopeHit, playerHeight * 0.5f + 0.3f))
        {
            Debug.Log("slope");
            float angle = Vector3.Angle(Vector3.up, slopeHit.normal);
            return angle < maxSlopeAngle && angle != 0;
        }

        return false;
    }

    private Vector3 GetSlopeMoveDirection()
    {
        return Vector3.ProjectOnPlane(moveDirection, slopeHit.normal).normalized;
    }
}

internal class MoveSettings
{
}