using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Run : MonoBehaviour
{
    public float xPoint = 3;
    public float yPoint = 3;
    public float ZPoint = 3;
    public float time = 3;
    // Start is called before the first frame update
    void Start()
    {
        transform.DOMove(new Vector3(xPoint, yPoint, ZPoint), time).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
