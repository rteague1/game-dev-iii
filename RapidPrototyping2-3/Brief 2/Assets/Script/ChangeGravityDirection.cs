using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGravityDirection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKey(keyCode.Space))
        //{
        //    Physics.gravity = new Vector3(0, 0, 0); 
        //}
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Physics.gravity = new Vector3(x,0,z);
    }
}
