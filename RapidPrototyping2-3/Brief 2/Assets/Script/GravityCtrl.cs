using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityCtrl : MonoBehaviour
{
    public GravityOrbit Gravity;
    private Rigidbody Rb;

    public float RotationSpeed = 20;
    // Start is called before the first frame update
    private void Start()
    {
        Rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        //if  (Gravity.gravityOn)
        //{
 if (Gravity)
        {
            Vector3 gravityUP = Vector3.zero;
            if (Gravity.FixedDirection)
            {
                gravityUP = Gravity.transform.up;
            }
            else
            {
                gravityUP = (transform.position - Gravity.transform.position).normalized;
            }
            //GravityUP = (transform.postition - Gravity.transform.position).normalized;
            Vector3 localUp = transform.up;
            Quaternion targetrotaion = Quaternion.FromToRotation(localUp, gravityUP) * transform.rotation;
            transform.up = Vector3.Lerp(transform.up, gravityUP, RotationSpeed * Time.deltaTime);

            Rb.AddForce((-gravityUP * Gravity.Gravity) * Rb.mass);
        }
        //}
        //else 
        //{
        //   gameObject.SetActive(false);
        //}
       
    }
    //// Update is called once per frame
    void Update()
    {
        
    }
}
